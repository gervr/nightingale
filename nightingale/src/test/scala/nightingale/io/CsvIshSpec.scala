package nightingale.io

import nightingale.Spec

import java.time.LocalDateTime

class CsvIshSpec extends Spec {

  "CsvIsh" should {
    "provide a list of the data lines only" in {
      val csv = CsvIsh(series).fromResource("test-csv.txt")
      val list = csv.list
      list shouldBe List(
        Series(LocalDateTime.parse("2022-03-21T00:00"), Vector(1.1, 2.1, 3.1)),
        Series(LocalDateTime.parse("2022-03-21T00:02"), Vector(1.2, 2.2, 3.2)),
        Series(LocalDateTime.parse("2022-03-21T00:04"), Vector(1.3, 2.3, 3.3)),
        Series(LocalDateTime.parse("2022-03-21T00:06"), Vector(1.4, 2.4, 3.4)),
        Series(LocalDateTime.parse("2022-03-21T00:08"), Vector(1.5, 2.5, 3.5))
      )
    }
    "provide a foldLeft" in {
      val csv = CsvIsh(series).fromResource("test-csv.txt")
      def add(stats: Stats, entry: Series): Stats = Stats(stats.count + 1, stats.sum + entry.values.sum)
      csv.leftFold(Stats(0, 0.0))(add) shouldBe Stats(5, 34.5)
    }
  }

  case class Series(timestamp: LocalDateTime, values: Vector[Double])

  case class Stats(count: Int, sum: Double)

  def series(array: Array[String]): Series =
    Series(LocalDateTime.parse(array(0).replace(' ', 'T')), array.tail.map(_.toDouble).toVector)

}
