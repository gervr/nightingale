package nightingale

class MainSpec extends Spec {

  "Main" should {
    "have a name" in {
      Main.name shouldBe "Nightingale"
    }
  }

}
