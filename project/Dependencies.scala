import sbt._

object Dependencies {

  private object versions {
    val jmh = "1.35"
    val scalatest = "3.2.13"
  }

  private val test: Seq[ModuleID] = Seq(
    "org.scalatest" %% "scalatest" % versions.scalatest % Test
  )

  private val bench: Seq[ModuleID] = Seq(
    "org.openjdk.jmh" % "jmh-core" % versions.jmh
  )

  val core: Seq[ModuleID] = test

  val benchmarks: Seq[ModuleID] = bench

}
