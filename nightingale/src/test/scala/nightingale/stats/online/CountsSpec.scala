package nightingale.stats.online

import nightingale.Spec
import nightingale.stats.online.Counts.Cumulative

class CountsSpec extends Spec {

  import CountsSpec._

  "Counts" should {
    "have an immutable class Counts that" should {
      "be creatable from a recorder" in {
        val recorder = Counts.recorder(5)
        val counts = recorder.read()
        counts.size shouldBe 5
      }
      "be creatable with underflow" in {
        val counts = withCounters(1, 2, 3).withUnderflow(4).getCounts
        counts.underflow shouldBe 4L
      }
      "be creatable with overflow" in {
        val counts = withCounters(1, 2, 3).withOverflow(5).getCounts
        counts.overflow shouldBe 5L
      }
      "have a size indicating the number of data bins" in {
        val counts = withCounters(1, 2, 3, 4, 3, 2, 1).getCounts
        counts.size shouldBe 7
      }
      "return counts per index" in {
        val counts = withCounters(10, 20, 30).withUnderflow(4).withOverflow(5).getCounts
        counts(-123) shouldBe 0L
        counts(-1) shouldBe 0L
        counts(3) shouldBe 0L
        counts(321) shouldBe 0L
      }
      "return count 0 for indices less than zero or equal to or larger than size" in {
        val counts = withCounters(10, 20, 30).getCounts
        counts(0) shouldBe 10L
        counts(1) shouldBe 20L
        counts(2) shouldBe 30L
      }
      "have a total equal to the sum of all counter values and underflow and overflow" in {
        withCounters(1, 2, 3).getCounts.total shouldBe 6L
        withCounters(1, 2, 3).withUnderflow(4).getCounts.total shouldBe 10L
        withCounters(1, 2, 3).withOverflow(5).getCounts.total shouldBe 11L
        withCounters(1, 2, 3).withUnderflow(4).withOverflow(5).getCounts.total shouldBe 15L
      }
      "have a total equal to underflow and overflow for zero counters" in {
        withCounters().getCounts.total shouldBe 0L
        withCounters().withUnderflow(4).getCounts.total shouldBe 4L
        withCounters().withOverflow(5).getCounts.total shouldBe 5L
        withCounters().withUnderflow(4).withOverflow(5).getCounts.total shouldBe 9L
      }
      "sum of counters from an index (inclusive) until an index (exclusive)" in {
        val counts = withCounters(1, 2, 3, 4).getCounts
        counts.sum(0, 4) shouldBe 10L
        counts.sum(0, 3) shouldBe 6L
        counts.sum(1, 3) shouldBe 5L
        counts.sum(1, 4) shouldBe 9L
      }
      "have default from 0 in sum of counters" in {
        val counts = withCounters(1, 2, 3, 4).getCounts
        counts.sum(until = 4) shouldBe 10L
        counts.sum(until = 2) shouldBe 3L
      }
      "have default until (size) in sum of counters" in {
        val counts = withCounters(1, 2, 3, 4).getCounts
        counts.sum(from = 1) shouldBe 9L
        counts.sum(from = 2) shouldBe 7L
      }
      "allow no arguments in sum" in {
        val counts = withCounters(1, 2, 3, 4).getCounts
        counts.sum() shouldBe 10L
      }
      "sum returns the sum of counters from an index (inclusive) until an index (exclusive)" in {
        val counts = withCounters(1, 2, 3, 4).getCounts
        counts.sum(0, 4) shouldBe 10L
        counts.sum(0, 3) shouldBe 6L
        counts.sum(1, 3) shouldBe 5L
        counts.sum(1, 4) shouldBe 9L
      }
      "totalUntil returns the sum of counters until the index (exclusive)" in {
        val counts = withCounters(1, 2, 3).getCounts
        counts.totalUntil(0) shouldBe 0L
        counts.totalUntil(1) shouldBe 1L
        counts.totalUntil(2) shouldBe 3L
        counts.totalUntil(3) shouldBe 6L
      }
      "totalUntil should include underflow and ignore overflow" in {
        val counts = withCounters(1, 2, 3).withUnderflow(4).withOverflow(5).getCounts
        counts.totalUntil(0) shouldBe 4L
        counts.totalUntil(1) shouldBe 5L
        counts.totalUntil(2) shouldBe 7L
        counts.totalUntil(3) shouldBe 10L
      }
      "totalFrom returns the sum of counters until the index (exclusive)" in {
        val counts = withCounters(1, 2, 3).getCounts
        counts.totalFrom(0) shouldBe 6L
        counts.totalFrom(1) shouldBe 5L
        counts.totalFrom(2) shouldBe 3L
        counts.totalFrom(3) shouldBe 0L
      }
      "totalFrom should include overflow and ignore underflow" in {
        val counts = withCounters(1, 2, 3).withUnderflow(4).withOverflow(5).getCounts
        counts.totalFrom(0) shouldBe 11L
        counts.totalFrom(1) shouldBe 10L
        counts.totalFrom(2) shouldBe 8L
        counts.totalFrom(3) shouldBe 5L
      }
      "allow for zero counters without underflow or overflow" in {
        val counts = withCounters().getCounts
        counts.size shouldBe 0
        counts.total shouldBe 0L
        counts.underflow shouldBe 0L
        counts.overflow shouldBe 0L
      }
      "add other counters" in {
        val a = withCounters(0, 1, 3).getCounts
        val b = withCounters(4, 2, 4).getCounts
        val expected = withCounters(4, 3, 7).getCounts
        a + b shouldMatch expected
        b + a shouldMatch expected
      }
      "add other counters with underflow and overflow" in {
        val a = withCounters(0, 1, 3).withUnderflow(2).withOverflow(3).getCounts
        val b = withCounters(4, 3, 2).withUnderflow(1).withOverflow(2).getCounts
        val expected = withCounters(4, 4, 5).withUnderflow(3).withOverflow(5).getCounts
        a + b shouldMatch expected
        b + a shouldMatch expected
      }
      "throw exception when trying to add unequal size counters" in {
        val a = withCounters(0, 1, 3).getCounts
        val b = withCounters(2, 4).getCounts
        intercept[AssertionError] {
          a + b
        }
        intercept[AssertionError] {
          b + a
        }
      }
      "provide immutable cumulative counts" in {
        val counts = withCounters(0, 1, 3).withUnderflow(2).withOverflow(3).getCounts
        val cum = counts.cumulative
        cum.size shouldBe 3
        cum.min shouldBe 2L
        cum(0) shouldBe 2L
        cum(1) shouldBe 3L
        cum(2) shouldBe 6L
        cum.max shouldBe 9L
      }
    }
    "have a mutable Recorder that" should {
      import Counts.Recorder

      "is constructed from a size" in {
        val recorder: Recorder = Counts.recorder(3)
        recorder.read().size shouldBe 3
      }
      "is initialized with zeros" in {
        val recorder = Counts.recorder(3)
        val counts = recorder.read()
        counts.size shouldBe 3
        counts.underflow shouldBe 0L
        counts(0) shouldBe 0L
        counts(1) shouldBe 0L
        counts(2) shouldBe 0L
        counts.overflow shouldBe 0L
        counts.total shouldBe 0L
      }
      "increment index by 1" in {
        val recorder = Counts.recorder(3)
        recorder.read().total shouldBe 0L
        recorder.increment(0)
        recorder.read().apply(0) shouldBe 1L
        recorder.read().total shouldBe 1L
        recorder.increment(0)
        recorder.read().apply(0) shouldBe 2L
        recorder.read().total shouldBe 2L
        recorder.increment(1)
        recorder.read().apply(1) shouldBe 1L
        recorder.read().total shouldBe 3L
      }
      "register negative indices as underflow" in {
        val recorder = Counts.recorder(3)
        recorder.read().underflow shouldBe 0L
        recorder.increment(-1)
        recorder.read().underflow shouldBe 1L
        recorder.increment(-123)
        recorder.read().underflow shouldBe 2L
        recorder.increment(Int.MinValue)
        recorder.read().underflow shouldBe 3L
      }
      "register indices equal to or larger than the size as overflow" in {
        val recorder = Counts.recorder(3)
        recorder.read().overflow shouldBe 0L
        recorder.increment(3)
        recorder.read().overflow shouldBe 1L
        recorder.increment(123)
        recorder.read().overflow shouldBe 2L
        recorder.increment(Int.MaxValue)
        recorder.read().overflow shouldBe 3L
      }
      "provide immutable counts" in {
        val recorder = Counts.recorder(7)
        val counts = recorder.read()
        counts.size shouldBe 7
        counts.total shouldBe 0L
        recorder.increment(0)
        recorder.read().total shouldBe 1L
        counts.total shouldBe 0L
      }
      "provide immutable cumulative counts" in {
        val recorder = Counts.recorder(7)
        val cum = recorder.cumulative()
        cum.size shouldBe 7
        cum.min shouldBe 0L
        recorder.increment(0)
        recorder.cumulative().max shouldBe 1L
        cum.max shouldBe 0L
      }
    }
    "have an immutable cumulative class" should {
      "cumulative should be the cumulative sum per index of counts" in {
        val cum = withCounters(1, 2, 3).getCumulative
        cum.min shouldBe 0L
        cum(0) shouldBe 1L
        cum(1) shouldBe 3L
        cum(2) shouldBe 6L
        cum.max shouldBe 6L
      }
      "cumulative should handle underflow and overflow" in {
        val cum = withCounters(1, 2, 3).withUnderflow(3).withOverflow(5).getCumulative
        cum.min shouldBe 3L
        cum(0) shouldBe 4L
        cum(1) shouldBe 6L
        cum(2) shouldBe 9L
        cum.max shouldBe 14L
      }
      "cumulative should return min for negative indices" in {
        val cum = withCounters(1, 2, 3).withUnderflow(3).withOverflow(5).getCumulative
        cum.min shouldBe 3L
        cum(-1) shouldBe 3L
        cum(-123) shouldBe 3L
        cum(Int.MinValue) shouldBe 3L
      }
      "cumulative should return max for indices equal to or larger than the size" in {
        val cum = withCounters(1, 2, 3).withUnderflow(3).withOverflow(5).getCumulative
        cum.max shouldBe 14L
        cum(3) shouldBe 14L
        cum(123) shouldBe 14L
        cum(Int.MaxValue) shouldBe 14L
      }
      "cumulative should handle zero size counts" in {
        val cum = withCounters().getCumulative
        cum.size shouldBe 0
        cum.min shouldBe 0L
        cum.max shouldBe 0L
      }
      "cumulative should handle zero size counts with underflow and overflow" in {
        val cum = withCounters().withUnderflow(3).withOverflow(1).getCumulative
        cum.size shouldBe 0
        cum.min shouldBe 3L
        cum.max shouldBe 4L
      }
      "cumulative should find the index i of a target for which cumulative(i - 1) < target <= cumulative(i)" in {
        val cum = withCounters(1, 2).getCumulative
        cum.find(1L) shouldBe 0
        cum.find(2L) shouldBe 1
        cum.find(3L) shouldBe 1
      }
      "find should handle even and odd sizes" in {
        withCounters(1).getCumulative.find(1) shouldBe 0
        withCounters(1, 1).getCumulative.find(1) shouldBe 0
        withCounters(1, 1).getCumulative.find(2) shouldBe 1
        withCounters(1, 1, 1).getCumulative.find(1) shouldBe 0
        withCounters(1, 1, 1).getCumulative.find(2) shouldBe 1
        withCounters(1, 1, 1).getCumulative.find(3) shouldBe 2
        withCounters(1, 1, 1, 1).getCumulative.find(1) shouldBe 0
        withCounters(1, 1, 1, 1).getCumulative.find(2) shouldBe 1
        withCounters(1, 1, 1, 1).getCumulative.find(3) shouldBe 2
        withCounters(1, 1, 1, 1).getCumulative.find(4) shouldBe 3
      }
      "find should handle zero increases (resulting in gaps)" in {
        val cum = withCounters(0, 1, 0, 2).getCumulative
        cum.find(1) shouldBe 1
        cum.find(2) shouldBe 3
        cum.find(3) shouldBe 3
      }
      "find should provide negative indices for targets smaller than or equal to the minimum" in {
        val cum = withCounters(1, 2).getCumulative
        cum.find(0) shouldBe -1
        cum.find(-100) shouldBe -1
        cum.find(Long.MinValue) shouldBe -1
      }
      "find should provide size as index for targets larger than cumulative(size-1)" in {
        val cum = withCounters(1, 2).getCumulative
        cum.find(4) shouldBe 2
        cum.find(40) shouldBe 2
        cum.find(Long.MaxValue) shouldBe 2
      }
      "find should handle underflow" in {
        val cum = withCounters(0, 1, 2).withUnderflow(3).getCumulative
        cum.find(0) shouldBe -1
        cum.find(1) shouldBe -1
        cum.find(3) shouldBe -1
        cum.find(4) shouldBe 1
        cum.find(5) shouldBe 2
        cum.find(6) shouldBe 2
      }
      "find should handle overflow (1)" in {
        val cum = withCounters(0, 1, 2).withUnderflow(1).withOverflow(2).getCumulative
        cum.find(2) shouldBe 1
        cum.find(4) shouldBe 2
        cum.find(5) shouldBe 3
        cum.find(6) shouldBe 3
        cum.find(7) shouldBe 3
      }
      "find should handle overflow (2)" in {
        val cum = withCounters(0, 1, 0, 2, 0, 0).withOverflow(2).getCumulative
        cum.find(3) shouldBe 3
        cum.find(4) shouldBe 6
        cum.find(5) shouldBe 6
        cum.find(6) shouldBe 6
      }
      "find should handle zero size counts" in {
        val cum = withCounters().getCumulative
        cum.find(0) shouldBe -1
        cum.find(1) shouldBe 0
      }
      "find should handle zero size counts with underflow and overflow" in {
        val cum = withCounters().withUnderflow(3).withOverflow(1).getCumulative
        cum.find(0) shouldBe -1
        cum.find(3) shouldBe -1
        cum.find(4) shouldBe 0
        cum.find(5) shouldBe 0
      }
      "find should handle size one counts" in {
        val cum = withCounters(2).withUnderflow(2).withOverflow(1).getCumulative
        cum.find(0) shouldBe -1
        cum.find(1) shouldBe -1
        cum.find(2) shouldBe -1
        cum.find(3) shouldBe 0
        cum.find(4) shouldBe 0
        cum.find(5) shouldBe 1
        cum.find(6) shouldBe 1
      }
      "cumulative should add other cumulative counts" in {
        val a = withCounters(2, 1).withUnderflow(2).withOverflow(1).getCumulative
        val b = withCounters(1, 5).withUnderflow(2).withOverflow(7).getCumulative
        val expected = withCounters(3, 6).withUnderflow(4).withOverflow(8).getCumulative
        a + b shouldMatch expected
        b + a shouldMatch expected
      }
      "add should not support unequal sizes" in {
        val a = withCounters(2, 1).withUnderflow(2).withOverflow(1).getCumulative
        val b = withCounters(1, 5, 3).withUnderflow(2).withOverflow(7).getCumulative
        intercept[AssertionError] {
          a + b
        }
        intercept[AssertionError] {
          b + a
        }
      }
      "add should handle zero size" in {
        val a = withCounters().withUnderflow(1).withOverflow(1).getCumulative
        val b = withCounters().withUnderflow(2).withOverflow(7).getCumulative
        val expected = withCounters().withUnderflow(3).withOverflow(8).getCumulative
        a + b shouldMatch expected
        b + a shouldMatch expected
      }
      "add should handle size 1" in {
        val a = withCounters(2).withUnderflow(1).withOverflow(1).getCumulative
        val b = withCounters(1).withUnderflow(2).withOverflow(7).getCumulative
        val expected = withCounters(3).withUnderflow(3).withOverflow(8).getCumulative
        a + b shouldMatch expected
        b + a shouldMatch expected
      }
    }
  }

}

object CountsSpec {

  import org.scalatest.matchers.should.Matchers._

  implicit private final class MatchCounts(val result: Counts) extends AnyVal {

    def shouldMatch(expected: Counts): Unit = {
      result.size shouldBe expected.size
      result.total shouldBe expected.total
      result.underflow shouldBe expected.underflow
      result.overflow shouldBe expected.overflow
      0 until result.size foreach { n =>
        result(n) shouldBe expected(n)
      }
    }

  }

  implicit private final class MatchCumulative(val result: Cumulative) extends AnyVal {

    def shouldMatch(expected: Cumulative): Unit = {
      result.size shouldBe expected.size
      result.min shouldBe expected.min
      result.max shouldBe expected.max
      0 until result.size foreach { n =>
        result(n) shouldBe expected(n)
      }
    }

  }

  private def withCounters(x: Int*): Builder = {
    val size = x.size
    val recorder = Counts.recorder(size)
    x.zipWithIndex foreach { case (value, index) =>
      incrementN(recorder, index, value)
    }
    new Builder(recorder, size)
  }

  private class Builder(recorder: Counts.Recorder, size: Int) {

    def withUnderflow(x: Int): Builder = {
      incrementN(recorder, -1, x)
      this
    }

    def withOverflow(x: Int): Builder = {
      incrementN(recorder, size, x)
      this
    }

    def getCounts: Counts = recorder.read()

    def getCumulative: Counts.Cumulative = recorder.cumulative()

  }

  private def incrementN(recorder: Counts.Recorder, index: Int, times: Int): Unit = {
    var n = 0
    while (n < times) {
      recorder.increment(index)
      n += 1
    }
  }

}
