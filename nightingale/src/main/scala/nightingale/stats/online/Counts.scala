package nightingale.stats.online

import nightingale.util.LongArrays

import scala.annotation.tailrec

final class Counts private (
    val underflow: Long,
    val overflow: Long,
    val total: Long,
    private val data: Array[Long]
) {

  import Counts._

  val size: Int = data.length

  @inline def apply(n: Int): Long =
    if (n >= 0 && n < size) data(n) else 0L

  @inline def sum(from: Int = 0, until: Int = size): Long =
    dataSum(from, until)

  @inline def totalUntil(n: Int): Long =
    underflow + dataSum(0, n)

  @inline def totalFrom(n: Int): Long =
    overflow + dataSum(n, size)

  @inline def +(that: Counts): Counts =
    Counts.add(this, that)

  @inline def cumulative: Cumulative =
    Cumulative.create(underflow, total, data)

  private def dataSum(from: Int, until: Int): Long = {
    var result = 0L
    var n = from
    while (n < until) {
      result += data(n)
      n += 1
    }
    result
  }

}

object Counts {

  @inline def recorder(size: Int): Recorder = new Recorder(size)

  // not threadsafe
  final class Recorder private[Counts] (size: Int) {

    private val data: Array[Long] = LongArrays.zeros(size)

    private var underflow: Long = 0L

    private var overflow: Long = 0L

    private var total: Long = 0L

    def increment(index: Int): Unit = {
      total += 1L
      if (index < 0) underflow += 1L
      else if (index < size) data(index) += 1L
      else overflow += 1L
    }

    private[Counts] def add(counts: Counts): Unit = {
      total += counts.total
      underflow += counts.underflow
      overflow += counts.overflow
      var n = 0
      while (n < size) {
        data(n) += counts.data(n)
        n += 1
      }
    }

    @inline def read(): Counts =
      Counts.create(underflow, overflow, total, data)

    @inline def cumulative(): Cumulative =
      Cumulative.create(underflow, total, data)

  }

  final class Cumulative private[Counts] (
      val min: Long,
      val max: Long,
      private val data: Array[Long]
  ) {

    val size: Int = data.length

    private val limit = if (size > 0) data(size - 1) else min

    @inline def apply(n: Int): Long = {
      if (n < 0) min
      else if (n < size) data(n)
      else max
    }

    @inline def +(that: Cumulative): Cumulative =
      Cumulative.add(this, that)

    @inline def find(target: Long): Int =
      if (target <= min) -1
      else if (target > limit) size
      else locate(target, -1, size - 1)

    @tailrec private def locate(target: Long, before: Int, to: Int): Int =
      if (before + 1 == to) to
      else {
        val split = (to + before) / 2
        if (target > data(split)) locate(target, split, to)
        else locate(target, before, split)
      }

  }

  private object Cumulative {

    final def create(underflow: Long, total: Long, data: Array[Long]): Cumulative = {
      val result = LongArrays.copy(data)
      if (result.length > 0) {
        result(0) += underflow
      }
      var n = 1
      while (n < result.length) {
        result(n) += result(n - 1)
        n += 1
      }
      new Cumulative(underflow, total, result)
    }

    @inline private def add(a: Cumulative, b: Cumulative): Cumulative =
      if (b.max == 0L) a
      else if (a.max == 0L) b
      else
        new Cumulative(
          min = a.min + b.min,
          max = a.max + b.max,
          data = LongArrays.add(a.data, b.data)
        )

  }

  @inline private def create(underflow: Long, overflow: Long, total: Long, data: Array[Long]): Counts = {
    new Counts(underflow, overflow, total, LongArrays.copy(data))
  }

  @inline private def add(a: Counts, b: Counts): Counts =
    if (b.total == 0L) a
    else if (a.total == 0L) b
    else
      new Counts(
        underflow = a.underflow + b.underflow,
        overflow = a.overflow + b.overflow,
        total = a.total + b.total,
        data = LongArrays.add(a.data, b.data)
      )

  final def merge(a: Counts, as: Counts*): Counts = {
    val sum = recorder(a.size)
    sum.add(a)
    as foreach sum.add
    sum.read()
  }

}
