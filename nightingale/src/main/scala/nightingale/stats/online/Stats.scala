package nightingale.stats.online

final class Stats {

}

object Stats {

  final class Recorder {

    private var total = 0L

    private var min = Double.NaN

    private var max = Double.NaN

    private var mean = Double.NaN

    private var m2 = Double.NaN

    def add(value: Double): Unit = {
      total += 1L
      if (total == 1L) {
        min = value
        max = value
        mean = value
        m2 = 0.0
      } else {
        if (value < min) min = value
        else if (value > max) max = value
        val delta = value - mean
        mean  += delta / total
        m2 += (value - mean) * delta
      }
    }


  }




}
