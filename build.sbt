ThisBuild / scalaVersion := "2.13.9"
ThisBuild / crossScalaVersions ++= Seq("2.13.9", "3.1.2")
ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings"
)
ThisBuild / version := "0.1.0-SNAPSHOT"

lazy val nightingale = (project in file("."))
  .aggregate(benchmarks, core, examples, lab)

lazy val core = (project in file("nightingale"))
  .settings(
    name := "nightingale",
    libraryDependencies ++= Dependencies.core
  )

lazy val benchmarks = (project in file("benchmarks"))
  .dependsOn(core)
  .settings(
    name := "benchmarks",
    libraryDependencies ++= Dependencies.benchmarks
  )
  .enablePlugins(JmhPlugin)

lazy val examples = (project in file("examples"))
  .dependsOn(core)
  .settings(
    name := "examples"
  )

lazy val lab = (project in file("lab"))
  .dependsOn(core)
  .settings(
    name := "lab"
  )
