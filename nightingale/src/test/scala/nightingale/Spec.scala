package nightingale

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

trait Spec extends AnyWordSpecLike with Matchers
