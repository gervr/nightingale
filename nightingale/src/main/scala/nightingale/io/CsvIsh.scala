package nightingale.io

import scala.annotation.tailrec
import scala.io.Source

trait CsvIsh[A] {

  def list: List[A]

  def leftFold[B](zero: B)(op: (B, A) => B): B

}

object CsvIsh {

  private val separator = ','

  private val comment = "#"

  def apply[A](f: Array[String] => A, skipHeader: Boolean = true): Builder[A] = new Builder[A](f, skipHeader)

  final class Builder[A](f: Array[String] => A, skipHeader: Boolean) {

    def fromFile(name: String): CsvIsh[A] = new Impl(() => Source.fromFile(name), f, skipHeader)

    def fromResource(name: String): CsvIsh[A] = new Impl(() => Source.fromResource(name), f, skipHeader)

  }

  final class Impl[A](open: () => Source, transform: Array[String] => A, skipHeader: Boolean) extends CsvIsh[A] {

    private def withSource[B](f: Iterator[A] => B): B = {
      val source = open()
      try {
        val iterator = new CsvIterator[A](source.getLines(), transform, skipHeader)
        f(iterator)
      } finally {
        source.close()
      }
    }

    override def list: List[A] = withSource { _.toList }

    override def leftFold[B](zero: B)(op: (B, A) => B): B = withSource { _.foldLeft(zero)(op) }

  }

  private final class CsvIterator[A](lines: Iterator[String], f: Array[String] => A, skipHeader: Boolean)
      extends Iterator[A] {

    private var line = {
      if (skipHeader && lines.hasNext) lines.next()
      nextDataLine()
    }

    private def nextDataLine(): Option[String] =
      if (lines.hasNext) nextLineLoop(lines.next())
      else None

    @tailrec
    private def nextLineLoop(string: String): Option[String] = {
      val skip = noDataLine(string)
      if (skip && lines.hasNext) nextLineLoop(lines.next())
      else if (!skip) Some(string)
      else None
    }

    private def noDataLine(line: String): Boolean = {
      val s = line.trim
      s.isEmpty || s.startsWith(comment)
    }

    private def parse(line: String): A = f(line.split(separator))

    override def hasNext: Boolean = line.nonEmpty

    override def next(): A = {
      val result = line.map(parse).getOrElse(throw new NoSuchElementException())
      line = nextDataLine()
      result
    }

  }

}
