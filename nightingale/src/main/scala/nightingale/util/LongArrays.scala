package nightingale.util

private[nightingale] object LongArrays {

  @inline final def zeros(size: Int): Array[Long] =
    Array.ofDim[Long](size)

  @inline final def copy(a: Array[Long]): Array[Long] = {
    val copy = Array.ofDim[Long](a.length)
    System.arraycopy(a, 0, copy, 0, a.length)
    copy
  }

  @inline final def add(a: Array[Long], b: Array[Long]): Array[Long] = {
    val size = a.length
    assert(size == b.length, "'add' is supported only for equal length arrays")
    val result = Array.ofDim[Long](size)
    var n = 0
    while (n < size) {
      result(n) = a(n) + b(n)
      n += 1
    }
    result
  }

}
