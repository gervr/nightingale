package nightingale.util

import nightingale.Spec

class LongArraysSpec extends Spec {

  "LongArrays.zeros" should {
    "create a new array filled with zero long values" in {
      val array = LongArrays.zeros(5)
      array.length shouldBe 5
      array foreach { element =>
        element shouldBe 0L
      }
    }
  }
  "LongArrays.copy" should {
    "copy arrays" in {
      val array = Array(0L, 11L, 202L)
      val copy = LongArrays.copy(array)
      copy.length shouldBe 3
      copy(0) shouldBe 0L
      copy(1) shouldBe 11L
      copy(2) shouldBe 202L
    }
    "be a deep copy (not affected by mutations on the origin)" in {
      val array = Array(0L, 11L, 202L)
      val copy = LongArrays.copy(array)
      array(1) = -23L
      array(2) = 0L
      copy(0) shouldBe 0L
      copy(1) shouldBe 11L
      copy(2) shouldBe 202L
    }
  }
  "LongArrays.add" should {
    "add values of two arrays" in {
      val a = Array(0L, 11L, 202L)
      val b = Array(3L, -10L, 1L)
      val sum = LongArrays.add(a, b)
      sum(0) shouldBe 3L
      sum(1) shouldBe 1L
      sum(2) shouldBe 203L
    }
    "not affect its originals" in {
      val a = Array(0L, 11L)
      val b = Array(3L, -10L)
      val sum = LongArrays.add(a, b)
      a(0) shouldBe 0L
      a(1) shouldBe 11L
      b(0) shouldBe 3L
      b(1) shouldBe -10L
    }
    "trow an exception when array lengths are not equal" in {
      val a = Array(0L, 11L)
      val b = Array(3L, -10L, 1L)
      intercept[AssertionError] {
        LongArrays.add(a, b)
      }
      intercept[AssertionError] {
        LongArrays.add(b, a)
      }
    }
  }

}
