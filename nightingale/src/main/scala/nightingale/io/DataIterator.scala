package nightingale.io

import scala.annotation.tailrec

object DataIterator {

  private val comment: String = "#"

  def wrap(lines: Iterator[String]): Iterator[String] = new Impl(lines)

  private final class Impl(lines: Iterator[String]) extends Iterator[String] {

    private var line = nextDataLine()

    private def nextDataLine(): Option[String] =
      if (lines.hasNext) nextLineLoop(lines.next())
      else None

    @tailrec
    private def nextLineLoop(string: String): Option[String] = {
      val skip = noDataLine(string)
      if (skip && lines.hasNext) nextLineLoop(lines.next())
      else if (!skip) Some(string)
      else None
    }

    private def noDataLine(line: String): Boolean = {
      val s = line.trim
      s.isEmpty || s.startsWith(comment)
    }

    override def hasNext: Boolean = line.nonEmpty

    override def next(): String = {
      val result = line.getOrElse(throw new NoSuchElementException())
      line = nextDataLine()
      result
    }

  }

}
